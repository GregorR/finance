from pathlib import Path
from typing import Any, Dict

import pytest
import yfinance as yf

from finalyzer.retrieval.stock_factory import Stock

TEST_ASSETS = Path(__file__).parent.absolute() / "assets"
DEFAULT_STOCK_PROPS = {
    "priceToBook": 2.0,
    "earnings": 1e6,
    "currentPrice": 22.0,
    "forwardPE": 15.0,
    "trailingPE": 17.0,
}


class DummyTicker(yf.Ticker):
    def __init__(self, ticker, stock_properties, *args, **kwargs):
        super().__init__(ticker, *args, **kwargs)
        self.stock_properties = stock_properties

    def set_info(self, key: str, value: Any):
        self.stock_properties[key] = value

    def get_info(self, proxy=None, as_dict=False, *args, **kwargs):
        return self.stock_properties

    def get_earnings(self, proxy=None, as_dict=False, freq="yearly"):
        return self.stock_properties["earnings"]


class DummyStock(Stock):
    def set_info(self, key, value):
        self._yfticker.set_info(key, value)


def __generate_dummy_stock(stock_properties: Dict):

    stock_properties["sector"] = "DUMMY_SECTOR"
    dummy_stock = DummyStock(ticker=DummyTicker("DUMMY", stock_properties=stock_properties))

    return dummy_stock


@pytest.fixture
def test_assets_dir():
    return TEST_ASSETS


@pytest.fixture
def dummy_stock():
    return __generate_dummy_stock(DEFAULT_STOCK_PROPS)


@pytest.fixture
def dummy_sector_stocks():
    props = [
        {"priceToBook": 1.0, "earnings": 1e3, "currentPrice": 10.0, "forwardPE": 15.0, "trailingPE": 17.0},
        {"priceToBook": 11.0, "earnings": 1e7, "currentPrice": 34.0, "forwardPE": 15.0, "trailingPE": 12.0},
        {"priceToBook": 21.0, "earnings": 1e9, "currentPrice": 143.50, "forwardPE": 17.0, "trailingPE": 11.0},
    ]
    dummy_stocks = [__generate_dummy_stock(prop) for prop in props]
    return dummy_stocks


@pytest.fixture
def energy_tickers():
    return ["XOM", "RYDAF", "CVX", "SHEL", "PTR", "COP", "TTE"]


@pytest.fixture
def dummy_yf_tickers():
    return [
        DummyTicker(ticker, DEFAULT_STOCK_PROPS)
        for ticker in ["XOM", "RYDAF", "CVX", "SHEL", "PTR", "COP", "TTE"]
    ]


@pytest.fixture
def get_fewer_tickers():
    from yfinance import Tickers

    def get_few(tickers, *args, **kwargs):
        return Tickers(tickers[:2], *args, **kwargs)

    return get_few
