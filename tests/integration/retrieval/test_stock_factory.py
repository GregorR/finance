"""
Created at 3/23/2022 9:27 PM
@author: Laptop
"""

from finalyzer.retrieval.stock_factory import get_stock_from_ticker


def test_stock_retrieves_info():

    stock = get_stock_from_ticker("AAPL")
    assert isinstance(stock.last_price, float)
    assert isinstance(stock.price_to_book, float)
    assert isinstance(stock.sector, str)

    stock = get_stock_from_ticker("NGS")
    print("fuck")
    pass
