"""
Created at 8/7/2022 3:43 PM
"""
from unittest.mock import patch

from typer.testing import CliRunner

from finalyzer.cli import cli

runner = CliRunner()


class TestAnalyze:
    def setup_class(self):
        self.sectors = ["consumer_cyclical", "real_estate", "energy"]

    def test_analyze(self, get_fewer_tickers):

        with patch("finalyzer.retrieval.core.yf.Tickers", get_fewer_tickers):
            runner.invoke(cli, ["analyze", *self.sectors])

    def test_analyze_w_sigma(self, get_fewer_tickers):

        with patch("finalyzer.retrieval.core.yf.Tickers", get_fewer_tickers):
            runner.invoke(cli, ["analyze", *self.sectors, "--sigma-factor", 2])
