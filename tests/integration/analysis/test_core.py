"""
Created at 7/23/2022 1:38 PM
"""

from unittest.mock import patch

from finalyzer.analysis.core import rate_sector
from finalyzer.analysis.raters.price_book import PriceToBookRater


def test_rate_sector(get_fewer_tickers):

    with patch("finalyzer.retrieval.core.yf.Tickers", get_fewer_tickers):
        rate_sector("energy", PriceToBookRater)
