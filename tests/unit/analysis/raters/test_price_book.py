"""
Created at 4/5/2021 6:56 PM
"""

import numpy as np

from finalyzer.analysis.raters.price_book import PriceToBookRater


class TestPriceBookRater:
    def test_absolute_score(self, dummy_stock, dummy_sector_stocks):
        pbrater = PriceToBookRater(dummy_sector_stocks, sector="dummy")

        # change rough specs of score dist
        assert pbrater._abs_dist_peak <= 1.0
        assert pbrater._abs_dist_peak > 0.85

        dummy_stock.set_info("priceToBook", pbrater._abs_dist_peak)
        abs_score = pbrater._absolute_rating(dummy_stock)
        assert np.isclose(abs_score, 1.0)

        dummy_stock.set_info("priceToBook", pbrater._abs_dist_peak * 0.9)
        abs_score = pbrater._absolute_rating(dummy_stock)
        assert abs_score < 1.0

        dummy_stock.set_info("priceToBook", pbrater._abs_dist_peak * 1.1)
        abs_score = pbrater._absolute_rating(dummy_stock)
        assert abs_score < 1.0

    def test_sector_score(self, dummy_stock, dummy_sector_stocks):
        pbrater = PriceToBookRater(dummy_sector_stocks, sector="dummy")

        sector_props = pbrater.set_properties
        assert sector_props.mean == np.mean([stock.price_to_book for stock in dummy_sector_stocks])
        assert sector_props.std == np.std([stock.price_to_book for stock in dummy_sector_stocks])
        assert sector_props.min == np.min([stock.price_to_book for stock in dummy_sector_stocks])

    def test_rating(self, dummy_stock, dummy_sector_stocks):
        pbrater = PriceToBookRater(dummy_sector_stocks, sector="dummy")

        sector_props = pbrater.set_properties
        dummy_stock.set_info("priceToBook", sector_props.dist_peak)
        best_rating = pbrater.analyse(dummy_stock)
        assert np.isclose(best_rating, 0.836, atol=1e-3)

        dummy_stock.set_info("priceToBook", sector_props.dist_peak * 1.1)
        rating = pbrater.analyse(dummy_stock)
        assert rating < best_rating
