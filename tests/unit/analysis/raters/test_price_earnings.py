"""
Created at 4/5/2021 6:56 PM
"""

import numpy as np

from finalyzer.analysis.raters.price_earnings import PriceEarningsRater


class TestPriceEarningsRater:
    def test_sector_score(self, dummy_stock, dummy_sector_stocks):
        rater = PriceEarningsRater(dummy_sector_stocks, sector="dummy")

        sector_props = rater.set_properties
        assert sector_props.mean == np.mean(
            [PriceEarningsRater._PriceEarningsRater__calc_pe(rater, stock) for stock in dummy_sector_stocks]
        )
        rating = rater.analyse(dummy_stock)
        assert np.isclose(rating, 0.1357, atol=1e-4)
