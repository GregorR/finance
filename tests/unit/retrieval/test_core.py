"""
Created at 3/23/2022 9:52 PM
@author: Laptop
"""

from unittest.mock import Mock, patch

from finalyzer.retrieval.core import get_sector_stocks, get_stocks


def test_get_stocks(energy_tickers, dummy_yf_tickers):

    with patch("finalyzer.retrieval.core.get_stock_from_ticker") as gsft_mock, patch(
        "finalyzer.retrieval.core.yf.Tickers"
    ) as tickers_mock:
        tickers_mock().tickers = dummy_yf_tickers
        get_stocks(*energy_tickers, sector="energy")
        assert gsft_mock.call_count == len(dummy_yf_tickers)


def test_get_sector_stocks(test_assets_dir):
    sector = "Energy"
    gs_mock = Mock(return_value="fine")

    with patch("finalyzer.retrieval.core.get_stocks", gs_mock):
        stocks = get_sector_stocks(sector, test_assets_dir)
        assert stocks == "fine"
        assert gs_mock.call_count == 1
        assert isinstance(gs_mock.call_args.args[0], str)
