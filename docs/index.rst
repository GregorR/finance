.. finalyzer documentation master file, created by
   sphinx-quickstart on Sat Mar 13 00:35:21 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to finalyzer's documentation!
=====================================

.. toctree::
   :glob:
   :maxdepth: 3
   :caption: Contents:

   _source/finalyzer.rst




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
