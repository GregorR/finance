# finalyzer Stock-Analysis Tool
![black badge](./assets/badge_black.svg)

The _finalyzer_ tool offers automatized sector-wise stock analysis based on fundamental values. Please read the DISCLAIMER before use.

---

### Introduction
The tool combines multiple (currently two) rating methods into a single sector-wise rating.
Each method produces a normalized rating and the final overall rating is consequently obtained as their
arithmetic mean. The analysis then presents the top-n rated stocks per sector, where n is set by the user.

### Installation
*finalyzer* is a Python-based CLI tool and provided as a Python package. To install:
1. Clone and switch to the repo.
2. Create and activate a dedicated virtual python environment.
3. Install the tool into that environment by running `pip install .`

If you would like to participate in the package development, replace step 3 by `pip install -e .[dev]`.
### Usage
After installation, the *finalyzer* CLI app is available in your virtual environment. You can call it simply
via `finalyze`. To kick off an analysis of, e.g., the energy and tech sectors, enter

```bash
finalyze analyze energy technology
```

Some raters produce a sector-relative rating by setting the peak score at a multiple of its standard deviation (sigma) 
from the set's mean. This multiple can be adjusted via the option `--sigma-factor` and defaults to one.    
To show help for the CLI enter a command without arguments or call with `--help`.

![cli_demo](./assets/cli_demo.gif)
*Fig. 1: CLI demonstration with a reduced set of sector stocks.*

### Data Provisioning
Stock data is obtained from yahoo finance. Currently, the full set of stocks in a sector is given as a
manually obtained list. Further, stocks with missing required information are excluded from the analyses.
The tool automatically creates a data cache by placing a `yf.cache` file in the working directory. This greatly
speeds up repeated queries.

### Rating Methods
The following two indicators are implemented currently.  
#### Price-Earnings Rater
The PE rater conducts a purely relative assessment of the given stock set's price-earnings ratios.
It assigns scores according to a Gaussian distribution $`\mathcal{N}`$ peaked at the set's mean $`\mu`$ less 
the multiplied (sigma factor $`\lambda`$) corresponding standard deviation $`\sigma`$:
```math
\mathcal{N}(\mu-\lambda\sigma, \sigma)
```
The idea here is that we want to find cheap assets according the PE ratio, however, are cautious about too cheap ones
as too low PE might indicate fundamental problems (e.g. expected earnings decline in the midterm) priced-in.
An upside could be realized, if a stock's PE deviation is merely an aberration or based on negligible circumstances
letting the stock's price return to the sectors' average in time.

Fig. 2 illustrates the PE rating for three example sets with sigma factor one. The considered PE ratio is
a 70/30 mix of forward and trailing PE:
```math
PE_{joint}=0.7*PE_{forward} + 0.3*PE_{trailing}
```

![demo_p_to_e](./assets/demo_price_earnings_ratings.svg)
*Fig. 2: The respective PE rating curve for three example sets a), b), c) of PE values in front of a histogram of the 
set. Black curve shows the rating, coral lines the set's
mean and mean minus one standard deviation. Darker bar color indicates a higher score. We can see that the 
score peaks according to above equation s.t. too high but also too low values are downgraded.*

#### Price-to-Book Rater
As shown in Fig. 3, the PB rater is the product of two independent factors: an absolute rating and a relative rating.
The absolute rating is given by a heavily rightwards-skewed Gaussian fixed just below a PB ratio of 1. This factor 
acts as a sort of envelope for the relative counterpart, strictly vanishing PB values below 0.75 and slowly downgrading
values above 1.
The rationale here is that the arbitrage should not allow for PB ratios significantly below 1 unless fundamental
concerns like fraud or imminent bankruptcy are priced-in. 

The relative score is derived analogously to the above described PE rating: It's taken from a Gaussian
centered at $`\mu-\lambda\sigma`$, where $`\mu`$ is the given value set's mean, $`\sigma`$ its standard deviation
and $`\lambda`$ a hyperparameter.
![demo_p_to_b](./assets/demo_price_book_ratings.svg)
*Fig. 3: a) The absolute rating factor with the peak marked in crimson. It acts as an envelope for the relative 
component, pulling down absolutely too low and high values. b)-e): Examples for the relative rating. This is 
analog to the PE rating: The plots show the rating curve in black in front the set's histogram. f)-g): The total
rating, which the product of the above two factors, for two further example sets of PB values. Note how the
best rating of a set is not necessarily 1, which is in contrast to the above PE rating. The absolute component can
absolutely downgrade a whole set.
Darker bar colors signify higher scores.*
