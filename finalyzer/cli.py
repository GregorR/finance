"""
Created at 3/26/2022 1:30 PM
@author: Laptop
"""
from pathlib import Path
from typing import List

import pandas as pd
import typer

from finalyzer.analysis.consolidation import create_analyses, infer_recommendations


SECTOR_CHOICES = [
    "basic_materials",
    "consumer_cyclical",
    "consumer_defensive",
    "energy",
    "healthcare",
    "real_estate",
    "technology",
    "utilities",
]
cli = typer.Typer(no_args_is_help=True)


@cli.command("clean-csv", no_args_is_help=True)
def clean_sector_csv(parent_dir: str = typer.Argument(...)):
    parent_dir = Path(parent_dir)
    for file in parent_dir.iterdir():
        if file.is_file() and file.suffix == ".csv" and file.name.startswith("sector"):
            df = pd.read_csv(parent_dir / file)
            tickers = df["Symbol"]
            tickers.to_frame().to_csv(file)


@cli.command("analyze", no_args_is_help=True)
def analyze(
    sectors: List[str] = typer.Argument(
        ..., help="Sectors for which stocks will be analyzed. " f"Choices:\n{', '.join(SECTOR_CHOICES)}."
    ),
    sigma_factor: float = typer.Option(
        1.0,
        "-s",
        "--sigma-factor",
        help="Factor for determining at how many std. devs. from the mean the max. relative rating shall sit.",
    ),
    n_recommendations: int = typer.Option(
        5, "-n", "--recommendations", help="Number of top-rated stocks to return."
    ),
    n_workers: int = typer.Option(
        2,
        "-w",
        "--n-workers",
        help="number of workers to use for analyses. will create simultaneous data queries.",
    ),
):
    df = create_analyses(sectors=sectors, sigma_factor=sigma_factor, n_workers=n_workers)
    recs = infer_recommendations(df, n_recommendations)
    pd.options.display.float_format = "{:,.2f}".format
    print(f"Established recommendations:\n{recs}")
