"""
Created at 3/11/2021 9:08 PM
"""
from abc import ABC, abstractmethod

from requests_cache import CachedSession


class Security(ABC):
    @property
    @abstractmethod
    def last_price(self):
        ...

    @property
    @abstractmethod
    def ask(self):
        ...

    @property
    @abstractmethod
    def ask_size(self):
        ...

    @property
    @abstractmethod
    def bid(self):
        ...

    @property
    @abstractmethod
    def bid_size(self):
        ...


def get_session():
    session = CachedSession("yf.cache")
    session.headers["User-agent"] = "finalyzer"
    return session
