"""
Created at 3/11/2021 9:10 PM
"""
from typing import List, Optional

import pandas as pd
import yfinance as yf
from tqdm import tqdm

import finalyzer.config as cf
from finalyzer.retrieval import get_session
from finalyzer.retrieval.stock_factory import Stock, get_stock_from_ticker


def get_stocks(*tickers: List[str], sector: Optional[str] = None) -> List[Stock]:
    print(f"Getting ticker information from yahoo finance for {sector}.")
    session = get_session()
    yf_tickers = yf.Tickers(list(tickers), session=session).tickers
    return [get_stock_from_ticker(ticker, sector=sector) for ticker in tqdm(yf_tickers)]


def get_sector_stocks(sector: str, assets_dir=cf.ASSETS_DIR) -> List[Stock]:
    """Handcrafted sector lists.

    Parameters
    ----------
    sector

    Returns
    -------

    """
    sector = sector.lower()
    tickers = pd.read_csv(assets_dir / f"sector_{sector}.csv", index_col=0)
    tickers = tickers["Symbol"].dropna().to_list()
    return get_stocks(*tickers, sector=sector)
