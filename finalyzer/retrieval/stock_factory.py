"""
Created at 3/11/2021 9:30 PM
"""
from typing import Union

import yfinance as yf

from finalyzer.retrieval import Security, get_session


class Stock(Security):
    def __init__(self, ticker: Union[str, yf.Ticker], sector: Union[None, str] = None):
        if isinstance(ticker, yf.Ticker):
            self.ticker = ticker.ticker
            self._yfticker = ticker
        elif isinstance(ticker, str):
            self.ticker = ticker
            self._yfticker = yf.Ticker(ticker, session=get_session())
        else:
            raise ValueError(f"'ticker' argument needs to be str or Ticker, found {type(ticker)}")

        if sector is None:
            self.sector = self.__get_info("sector")
        else:
            self.sector = sector.lower()

    def __get_info(self, key: str):
        return self._yfticker.get_info().get(key)

    @property
    def name(self):
        return self.__get_info("longName")

    @property
    def website(self):
        return self.__get_info("website")

    @property
    def last_price(self):
        return self.__get_info("regularMarketPrice")

    @property
    def ask(self):
        return self.__get_info("ask")

    @property
    def ask_size(self):
        return self.__get_info("askSize")

    @property
    def bid(self):
        return self.__get_info("bid")

    @property
    def bid_size(self):
        return self.__get_info("bidSize")

    @property
    def two_hundred_day_average(self):
        return self.__get_info("twoHundredDayAverage")

    @property
    def price_to_book(self):
        return self.__get_info("priceToBook")

    @property
    def trailing_price_earnings_ratio(self):
        return self.__get_info("trailingPE")

    @property
    def forward_price_earnings_ratio(self):
        return self.__get_info("forwardPE")

    @property
    def trailing_earnings_per_share(self):
        return self.__get_info("trailingEPS")

    @property
    def forward_earnings_per_share(self):
        return self.__get_info("forwardEPS")

    @property
    def earnings(self):
        return self._yfticker.get_earnings()

    @property
    def peg_ratio(self):
        return self.__get_info("pegRatio")


def get_stock_from_ticker(ticker: str, sector: Union[None, str] = None) -> Stock:
    return Stock(ticker, sector=sector)
