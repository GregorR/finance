"""
"""
from pathlib import Path
from typing import Dict, Iterable, List, Union

import numpy as np
from easydict import EasyDict
from matplotlib import cm
from matplotlib import gridspec as gs
from matplotlib import pyplot as plt

import finalyzer.config as cf

plt.rcParams.update({"text.usetex": True})


def __generate_comparison_set(property_dist: Dict[str, Iterable], n_samples: int = 20) -> List:
    """
    Parameters
    ----------
    property_dist: Dict
        dict with str keys representing stock property names and corresponding values holding an indexable item
        providing values for the key.
    n_samples

    Returns
    -------
    list of stocks
    """

    stocks = []
    for ix in range(n_samples):
        stocks.append(EasyDict({prop_name: dist[ix] for prop_name, dist in property_dist.items()}))

    return stocks


def plot_price_book_ratings_dist(outfile: Union[None, str, Path] = None):

    from finalyzer.analysis.raters.price_book import PriceToBookRater

    rater = PriceToBookRater(None, "dummy")
    fig = plt.figure(figsize=(12, 7), constrained_layout=True)
    grid = gs.GridSpec(5, 2, figure=fig)
    abs_dist_peak_color = "crimson"

    # absolute rating dist
    ax = fig.add_subplot(grid[0, :])
    x = np.linspace(0.5, 2.5, 100)
    x_distance = x.max() - x.min()
    absolute_ratings = [rater._absolute_rating(EasyDict({"price_to_book": val})) for val in x]
    ax.plot(x, absolute_ratings, "k-", lw=3)
    ax.set_title("a) Absolute Rating $r_{abs}$ vs. P-to-B $p$")
    ax.set_ylabel("Rating")
    ax.set_xlabel("P-to-B ratio")
    ax.axvline(
        x=rater._abs_dist_peak,
        alpha=1,
        color=abs_dist_peak_color,
        linewidth=3,
        linestyle="dashed",
        label="dist peak",
    )
    bbox = {"boxstyle": "square", "linewidth": 0, "facecolor": abs_dist_peak_color, "alpha": 0.8}
    ax.text(rater._abs_dist_peak + x_distance * 0.007, 0.5, "peak absolute rating", bbox=bbox)

    def plot_twinx_rating(ax, dist: np.ndarray, rating_func: str, n_samples: int, cmap=None):
        if cmap is None:
            cmap = cm.get_cmap("Blues")
        rater = PriceToBookRater(None, "dummy")
        rater.comparison_set = __generate_comparison_set({"price_to_book": dist}, n_samples=n_samples)

        x_axis = np.linspace(max(0.0, dist.min()), dist.max(), 100)
        ratings = [getattr(rater, rating_func)(EasyDict({"price_to_book": val})) for val in x_axis]
        ax.plot(x_axis, ratings, "k-", lw=3)
        ax.axvline(x=rater.set_properties.mean, color="coral", lw=3, linestyle="dotted")
        ax.axvline(
            x=rater.set_properties.mean - rater.set_properties.std, color="coral", lw=3, linestyle="dashed"
        )
        ax.set_ylabel("Rating (black)")
        ax2 = ax.twinx()

        pb_vals = [s.price_to_book for s in rater.comparison_set]
        n, bins, patches = ax2.hist(pb_vals, bins=25, weights=[1.0 / len(pb_vals)] * len(pb_vals))
        bar_positions = 0.5 * (bins[:-1] + bins[1:])
        for bar_p_to_b, patch in zip(bar_positions, patches):
            rating = getattr(rater, rating_func)(EasyDict({"price_to_book": bar_p_to_b}))
            plt.setp(patch, "facecolor", cmap(rating), alpha=0.9)
        ax2.set_ylabel("Quota (blue)")

        ax.set_zorder(ax2.get_zorder() + 1)
        ax.set_frame_on(False)
        ax.set_xlabel("P-to-B ratio")

        return rater

    rng = np.random.default_rng(seed=123)
    n_samples = 50
    ax = fig.add_subplot(grid[1, 0])
    pb_normal = rng.normal(loc=1.7, scale=0.4, size=n_samples)
    rater = plot_twinx_rating(ax, pb_normal, rating_func="_set_rating", n_samples=n_samples)

    bbox = {"boxstyle": "square", "linewidth": 0, "facecolor": "coral"}
    ax.text(rater.set_properties.mean + 0.05, 0.75, r"stocks set mean $\mu$", bbox=bbox)
    ax.text(rater.set_properties.mean - rater.set_properties.std + 0.05, 0.75, r"$\mu-\sigma$", bbox=bbox)
    ax.set_title(r"b) Relative Rating $r_{rel}$ vs. P-to-B $p$")

    ax = fig.add_subplot(grid[1, 1])
    pb_normal = rng.normal(loc=1.7, scale=1.1, size=n_samples)
    plot_twinx_rating(ax, pb_normal, rating_func="_set_rating", n_samples=n_samples)
    ax.set_title(r"c)Relative Rating $r_{rel}$ vs. P-to-B $p$")

    ax = fig.add_subplot(grid[2, 0])
    pb_normal = rng.uniform(low=0.1, high=8, size=n_samples)
    plot_twinx_rating(ax, pb_normal, rating_func="_set_rating", n_samples=n_samples)
    ax.set_title(r"d) Relative Rating $r_{rel}$ vs. P-to-B $p$")

    ax = fig.add_subplot(grid[2, 1])
    pb_normal = rng.uniform(low=0.5, high=3.0, size=n_samples)
    plot_twinx_rating(ax, pb_normal, rating_func="_set_rating", n_samples=n_samples)
    ax.set_title(r"e) Relative Rating $r_{rel}$ vs. P-to-B $p$")

    # total rating
    ax = fig.add_subplot(grid[3, :])
    n_samples = 150
    pb_normal = rng.normal(loc=0.98, scale=0.015, size=n_samples)
    plot_twinx_rating(ax, pb_normal, rating_func="analyse", n_samples=n_samples)
    ax.axvline(
        x=rater._abs_dist_peak,
        alpha=0.7,
        color=abs_dist_peak_color,
        linewidth=3,
        linestyle="dashed",
        label="dist peak",
    )
    ax.set_title(r"f) Total Rating $r$ vs. P-to-B $p$")

    ax = fig.add_subplot(grid[4, :])
    n_samples = 150
    pb_normal = rng.normal(loc=2.55, scale=0.1, size=n_samples)
    plot_twinx_rating(ax, pb_normal, rating_func="analyse", n_samples=n_samples)
    ax.axvline(
        x=rater._abs_dist_peak,
        alpha=0.7,
        color=abs_dist_peak_color,
        linewidth=3,
        linestyle="dashed",
        label="dist peak",
    )
    ax.set_title(r"g) Total Rating $r$ vs. P-to-B $p$")

    suptitle = (
        r"Price-to-Book Rating $r$ Components"
        "\n"
        r"$r=r_{abs} * r_{rel}$"
        "\n"
        r"$r_{abs}(p)=\mathcal{N}_{skewed}(p)$, $r_{rel}(p)=\mathcal{N}_{\mu - \sigma}(p)$"
    )
    plt.suptitle(suptitle)

    if outfile is not None:
        outfile = Path(outfile)
        outfile.parent.mkdir(exist_ok=True, parents=True)
        plt.savefig(outfile, dpi=300, bbox_inches="tight")


def plot_price_earnings_ratings_dist(outfile: Union[None, str, Path] = None):

    from finalyzer.analysis.raters.price_earnings import PriceEarningsRater

    def plot_twinx_rating(ax, dist: np.ndarray, n_samples: int = 50, cmap=None) -> PriceEarningsRater:
        if cmap is None:
            cmap = cm.get_cmap("Blues")
        rater = PriceEarningsRater(None, "dummy")
        pe_scaler = rater.forward_pe_weight
        rater.comparison_set = __generate_comparison_set(
            {
                "forward_price_earnings_ratio": dist / pe_scaler,
                "trailing_price_earnings_ratio": [0.0] * n_samples,
            },
            n_samples=n_samples,
        )

        x_axis = np.linspace(max(0.0, dist.min()), dist.max(), 100)
        stocks = [
            EasyDict({"forward_price_earnings_ratio": val / pe_scaler, "trailing_price_earnings_ratio": 0.0})
            for val in x_axis
        ]
        ratings = [rater.analyse(stock) for stock in stocks]
        ax.plot(x_axis, ratings, "k-", lw=3)
        ax.axvline(x=rater.set_properties.mean, color="coral", lw=3, linestyle="dotted")
        ax.axvline(
            x=rater.set_properties.mean - rater.set_properties.std, color="coral", lw=3, linestyle="dashed"
        )
        ax.set_ylabel("Rating (black)")
        ax2 = ax.twinx()

        pb_vals = [s.forward_price_earnings_ratio for s in rater.comparison_set]
        n, bins, patches = ax2.hist(pb_vals, bins=25, weights=[1.0 / len(pb_vals)] * len(pb_vals))
        bar_positions = 0.5 * (bins[:-1] + bins[1:])
        for bar_p_e, patch in zip(bar_positions, patches):
            stock = EasyDict(
                {"forward_price_earnings_ratio": bar_p_e / pe_scaler, "trailing_price_earnings_ratio": 0.0}
            )
            rating = rater.analyse(stock)
            plt.setp(patch, "facecolor", cmap(rating), alpha=0.9)
        ax2.set_ylabel("Quota (blue)")

        ax.set_zorder(ax2.get_zorder() + 1)
        ax.set_frame_on(False)
        ax.set_xlabel("Joint P-E ratio $PE_{joint}$")

        return rater

    fig = plt.figure(figsize=(12, 7), constrained_layout=True)
    grid = gs.GridSpec(2, 2, figure=fig)
    rng = np.random.default_rng(seed=121)
    n_samples = 100

    ax = fig.add_subplot(grid[0, :])
    pb_normal = rng.normal(loc=16, scale=5.5, size=n_samples)
    rater = plot_twinx_rating(ax, pb_normal, n_samples=n_samples)
    bbox = {"boxstyle": "square", "linewidth": 0, "facecolor": "coral"}
    ax.text(rater.set_properties.mean + 0.5, 0.75, r"stocks set mean $\mu$", bbox=bbox)
    ax.text(rater.set_properties.mean - rater.set_properties.std + 0.5, 0.75, r"$\mu-\sigma$", bbox=bbox)
    ax.set_title(r"a) Relative Rating $r$ vs. P-E $PE_{joint}$")

    ax = fig.add_subplot(grid[1, 0])
    pb_normal = rng.normal(loc=16, scale=7.5, size=n_samples)
    pb_normal = pb_normal[pb_normal > 0]
    plot_twinx_rating(ax, pb_normal, n_samples=len(pb_normal))
    ax.set_title(r"b) Relative Rating $r$ vs. P-E $PE_{joint}$")

    ax = fig.add_subplot(grid[1, 1])
    uniform = rng.uniform(low=2.0, high=22.5, size=n_samples)
    plot_twinx_rating(ax, uniform, n_samples=n_samples)
    ax.set_title(r"c) Relative Rating $r$ vs. P-E $PE_{joint}$")

    suptitle = (
        r"Price-to-Earnings Rating $r$"
        "\n"
        r"$r=\mathcal{N}_{\mu-\sigma, \sigma}(PE_{joint})$"
        "\n"
        r"$\mu=$ set mean, $ \sigma=$ set std. dev., $PE_{joint}=0.7*PE_{forward}+0.3*PE_{trailing}$"
    )
    plt.suptitle(suptitle)

    if outfile is not None:
        outfile = Path(outfile)
        outfile.parent.mkdir(exist_ok=True, parents=True)
        plt.savefig(outfile, dpi=300, bbox_inches="tight")


if __name__ == "__main__":

    plot_price_book_ratings_dist(outfile=cf.OUTPUTS_DIR / "demo_price_book_ratings.svg")
    plot_price_earnings_ratings_dist(outfile=cf.OUTPUTS_DIR / "demo_price_earnings_ratings.svg")
    pass
