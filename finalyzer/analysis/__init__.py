"""
Created at 3/11/2021 9:08 PM
@author: Laptop
"""

from abc import ABC, abstractmethod


class Analyzer(ABC):
    @abstractmethod
    def _consolidate(self):
        ...

    @abstractmethod
    def analyse(self):
        ...
