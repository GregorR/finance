"""
Created at 3/13/2021 1:52 PM
"""

import pandas as pd

from finalyzer.analysis.raters import AnalysisModule
from finalyzer.retrieval.core import get_sector_stocks


def rate_sector(
    sector: str, *rater_classes: AnalysisModule.__class__, sigma_factor: float = 1.0, n_workers: int = 2
) -> pd.DataFrame:

    stocks = get_sector_stocks(sector)

    df = pd.DataFrame({"ticker": [stock.ticker for stock in stocks]})
    df = df.set_index("ticker")
    for rater_class in rater_classes:
        rater = rater_class(stocks, sector=sector, sigma_factor=sigma_factor)
        ratings = rater.analyse_set(n_workers=n_workers)
        df = df.join(ratings.set_index("ticker"), how="left")
    return df
