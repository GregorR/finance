"""
Collect and consolidate all info in single pandas df
Created at 3/17/2021 8:33 PM
"""
from typing import List

import pandas as pd

from finalyzer.analysis.core import rate_sector
from finalyzer.analysis.raters.price_book import PriceToBookRater
from finalyzer.analysis.raters.price_earnings import PriceEarningsRater


def create_analyses(sectors: List[str], sigma_factor: float = 1.0, n_workers: int = 2) -> pd.DataFrame:

    df = pd.DataFrame(columns=["ticker"])
    df = df.set_index("ticker")

    raters = [PriceEarningsRater, PriceToBookRater]
    rating_cols = [rater.column_name for rater in raters]

    for sector in sectors:
        sector_ratings = rate_sector(sector, *raters, sigma_factor=sigma_factor, n_workers=n_workers)
        sector_ratings.loc[:, "sector"] = sector
        df = pd.concat((df, sector_ratings))

    df.loc[:, "mean_rating"] = df[rating_cols].fillna(value=0.0).mean(axis=1)
    return df


def infer_recommendations(analyses: pd.DataFrame, count: int):

    recs = analyses.sort_values(by=["sector", "mean_rating"], ascending=False)
    recs = recs.groupby("sector").head(count)

    return recs
