"""
Created at 7/23/2022 2:07 PM
"""
from typing import List

import numpy as np
import pandas as pd
from easydict import EasyDict
from scipy.stats import norm
from tqdm import tqdm

from finalyzer.analysis.raters import AnalysisModule
from finalyzer.retrieval.core import Stock


class PriceEarningsRater(AnalysisModule):
    """Create a rating for given set of stocks taking into consideration price relative to earnings."""

    _col_name = "price_earnings_rating"

    def __init__(self, stocks: List[Stock], sector: str, sigma_factor: float = 1.0):
        super().__init__(stocks)
        self.sector = sector
        self.sigma_factor = sigma_factor
        self.forward_pe_weight = 0.7
        self.trailing_pe_weight = 1.0 - self.forward_pe_weight
        self.__set_properties = None

    @classmethod
    @property
    def column_name(cls):
        return cls._col_name

    def __calc_pe(self, stock: Stock):
        try:
            fw_pe = stock.forward_price_earnings_ratio
            tl_pe = stock.trailing_price_earnings_ratio
            if fw_pe < 0 or tl_pe < 0:
                return float("nan")

            return self.forward_pe_weight * fw_pe + self.trailing_pe_weight * tl_pe
        except TypeError:
            return float("nan")

    @property
    def set_properties(self):
        if self.__set_properties is None:
            print(f"PE rater: Retrieving values of interest for {self.sector} stocks.")
            set_values = np.array([self.__calc_pe(s) for s in tqdm(self.comparison_set)])
            set_values = set_values[~np.isnan(set_values)]
            count_dropped = len(self.comparison_set) - len(set_values)
            if count_dropped > 0:
                print(
                    f"Dropping {count_dropped}/{len(self.comparison_set)} stocks "
                    f"from P-E rating due to missing info."
                )
            self.__set_properties = EasyDict()
            self.__set_properties.mean = np.mean(set_values)
            self.__set_properties.std = np.std(set_values)
            self.__set_properties.dist_peak = (
                self.__set_properties.mean - self.sigma_factor * self.__set_properties.std
            )
            self.__set_properties.dist = norm(
                loc=self.__set_properties.dist_peak, scale=self.__set_properties.std
            )
            self.__set_properties.dist_norm = self.__calc_set_score(self.__set_properties.dist_peak)

        return self.__set_properties

    def __calc_set_score(self, price_to_earnings: float) -> float:
        """The rating of a price-earnings ratio is the best one standard deviation below the comparison average.
        The rating is highest for :math:`\lambda` standard deviations :math:`\sigma` from the average :math:`\mu`.
        :math:`\lambda` can be configured by the user.
        Why not the smaller, the better? -> This is an isolated indicator, it does not consider the economic situation
        of the asset of interest. Too low PE ratios might indicate fundamental deficit, e.g., an imminent bankruptcy.
        Hence, PE rating decreases again with decreasing PE at one std dev.

        Parameters
        ----------
        stock: Stock

        Returns
        -------
        float
            normed rating

        """

        return self.set_properties.dist.pdf(price_to_earnings)

    def _set_rating(self, stock: Stock) -> float:
        """Return normalized stock rating considering its price-to-earnings ratio relative to set peers.

        Parameters
        ----------
        stock

        Returns
        -------
        float
            normalized rating of PE relative to set

        """

        score = self.__calc_set_score(self.__calc_pe(stock))

        return score / self.set_properties.dist_norm

    def analyse(self, stock: Stock) -> float:
        """Get normalized rating for p-to-e :math:`p` of given :param:`stock`.

        The rating is calculated from a normal distribution around the mean p-e :math:`\mu` of the set subtracted by one
        corresponding standard deviation :math:`\sigma`.

        set rating:
        ..math:: \text{score} = \mathcal{N}_{\mu-\sigma, \sigma}(p)

        Parameters
        ----------
        stock

        Returns
        -------
        float
            normalized rating
        """

        return self._set_rating(stock)

    def analyse_set(self, n_workers: int = 2) -> pd.DataFrame:
        analyses = super().analyse_set(n_workers=n_workers)
        df = pd.DataFrame({"ticker": analyses[0], "price_earnings_rating": analyses[1]})
        return df

    # calc and save new comparison values/benchmarks to db or get them from db (e.g., set-specific)
