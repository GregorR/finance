"""
Created at 3/17/2021 8:22 PM
priceToBook
"""
from typing import List, Union

import numpy as np
import pandas as pd
from easydict import EasyDict
from scipy.stats import norm, skewnorm
from tqdm import tqdm

from finalyzer.analysis.raters import AnalysisModule
from finalyzer.retrieval.core import Stock


class PriceToBookRater(AnalysisModule):
    """Create a price-book rating for given set of stocks."""

    _col_name = "price_book_rating"

    def __init__(self, stocks: List[Stock], sector: str, sigma_factor: float = 1.0):
        super().__init__(stocks)
        self.sector = sector
        self.sigma_factor = sigma_factor

        self._abs_dist_loc = 0.725
        self._abs_dist_scale = 3.5
        self._abs_dist = skewnorm(a=45, loc=self._abs_dist_loc, scale=self._abs_dist_scale)
        peak_search_space = np.linspace(
            self._abs_dist_loc - self._abs_dist_scale, self._abs_dist_loc + self._abs_dist_scale, 1000
        )
        peak_probes = self._abs_dist.pdf(peak_search_space)
        self._abs_dist_peak = peak_search_space[peak_probes.argmax()]
        self._abs_dist_norm = self._abs_dist.pdf(self._abs_dist_peak)

        self.__set_properties = None

    def __none_to_nan(self, val: Union[float, None]):
        if val is None:
            return float("nan")
        return val

    @classmethod
    @property
    def column_name(cls):
        return cls._col_name

    @property
    def set_properties(self):
        if self.__set_properties is None:
            print(f"PB rater: Retrieving values of interest for {self.sector} stocks.")
            set_values = np.array([self.__none_to_nan(s.price_to_book) for s in tqdm(self.comparison_set)])
            set_values = set_values[~np.isnan(set_values)]
            count_dropped = len(self.comparison_set) - len(set_values)
            if count_dropped > 0:
                print(
                    f"Dropping {count_dropped}/{len(self.comparison_set)} stocks "
                    f"from P-to-B rating due to missing info."
                )
            self.__set_properties = EasyDict()
            self.__set_properties.min = np.min(set_values)
            self.__set_properties.mean = np.mean(set_values)
            self.__set_properties.std = np.std(set_values)
            self.__set_properties.dist_peak = (
                self.__set_properties.mean - self.sigma_factor * self.__set_properties.std
            )
            self.__set_properties.dist = norm(
                loc=self.__set_properties.dist_peak, scale=self.__set_properties.std
            )
            self.__set_properties.dist_norm = self.__calc_set_score(self.__set_properties.dist_peak)
        return self.__set_properties

    def __calc_absolute_rating(self, price_to_book: float) -> float:
        return self._abs_dist.pdf(price_to_book) / self._abs_dist_norm

    def _absolute_rating(self, stock: Stock) -> float:
        """Scores the price-to-book ratio (p-b) on a strongly skewed normal distribution peaking just below 1.

        :attr:`self._abs_dist` provides a normal distribution skewed heavily to the right, so that it peaks
        barely below one, diminishes quickly to the left and slowly to the right.

        This is rooted in the assumption that a lower p-b is generally a sign for undervaluation, with upper bounds
        set softly somewhere around three. On the lower end, there is a sharper presumed limit, setting in quickly
        below one. My rationale behind this is trust in a working arbitrage mechanism in the observed market. This
        means, that a p-b significantly below one should exist in the market due to arbitrage - except if
        participants expect major devaluation of assets as, e.g., in the case of a looming bankruptcy or fraud.

        Parameters
        ----------
        stock: Stock

        Returns
        -------
        float
            normed rating

        """
        return self.__calc_absolute_rating(self.__none_to_nan(stock.price_to_book))

    def __calc_set_score(self, price_to_book: float) -> float:
        """Calc unnormalized score from p-to-b :math:`p`, set mean :math:`\mu` and standard deviation :math:`\sigma`.
        The set score is :math:`p` drawn from the set distribution, which is given by the following Gaussian
        :math:\mathcal{N}:

        ..math:: \text{score}(p) = \mathcal{N}_{\mu-\lambda*\sigma}(p),
        where :math:`\lambda` is the configured sigma factor.

        I.e., the score is maximal one standard dev. below the set's mean.

        Parameters
        ----------
        price_to_book: float

        Returns
        -------
        float
            unnormalized score
        """

        return self.set_properties.dist.pdf(price_to_book)

    def _set_rating(self, stock: Stock) -> float:
        """Return normalized stock rating considering its price-to-book ratio relative to set peers.

        Parameters
        ----------
        stock

        Returns
        -------
        float
            normalized rating of p-to-b relative to set

        """
        score = self.__calc_set_score(self.__none_to_nan(stock.price_to_book))
        return score / self.set_properties.dist_norm

    def analyse(self, stock: Stock) -> float:
        """Get rating from p-to-b of given :param:`stock`.

        Total rating is the product of absolute rating and set rating.
        This rating has an absolute scale - i.e., the total rating is not normed according to the set, but to an
        absolute scale. Hence, there is no guarantee the set's best rating is 1.0.

        The absolute factor acts as an envelope, confining the scope for relative ratings. E.g., a high relative
        rating in a near-zero absolute region still equals nearly zero.

        Parameters
        ----------
        stock: Stock

        Returns
        -------
        float
            absolutely normalized rating
        """

        return self._absolute_rating(stock) * self._set_rating(stock)

    def analyse_set(self, n_workers: int = 2) -> pd.DataFrame:
        analyses = super().analyse_set(n_workers=n_workers)
        df = pd.DataFrame({"ticker": analyses[0], self.column_name: analyses[1]})
        return df
