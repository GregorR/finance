"""
Created at 3/17/2021 8:32 PM
"""
from abc import ABC, abstractmethod
from multiprocessing import Pool
from typing import List, Tuple

from finalyzer.retrieval import Security


class AnalysisModule(ABC):
    def __init__(self, securities: List[Security]):
        self.comparison_set = securities

    @property
    @abstractmethod
    def column_name(self):
        raise NotImplementedError

    @classmethod
    @property
    @abstractmethod
    def set_properties(cls):
        raise NotImplementedError

    @property
    def set_tickers(self):
        return [sec.ticker for sec in self.comparison_set]

    @abstractmethod
    def analyse(self, security: Security) -> float:
        raise NotImplementedError

    def analyse_set(self, n_workers: int = 2) -> Tuple:

        pool = Pool(processes=n_workers)
        self.set_properties
        analyses = pool.map(self.analyse, self.comparison_set)
        return self.set_tickers, analyses
