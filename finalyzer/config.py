"""
Created at 7/21/2022 6:08 PM
"""
from pathlib import Path

REPO_ROOT = Path(__file__).absolute().parent.parent
ASSETS_DIR = REPO_ROOT / "assets"
OUTPUTS_DIR = REPO_ROOT / "outputs"
