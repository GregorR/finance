"""
Created at 3/8/2021 10:01 PM
@author: Gregor Ramien
"""

import os
from pathlib import Path

from setuptools import find_packages, setup


def parse_requirements(filepath):
    """Read requirements file in the style as produced by pip freeze.
    :param filepath: path to the requirements file.
    :return:
    """
    with open(filepath, "r") as f:
        lineiter = (line.strip() for line in f)
        reqs = [line for line in lineiter if line and not line.startswith("#")]
    return reqs


def get_version(filepath):
    with open(filepath, "r") as f:
        for line in f:
            line = line.strip()
            if line.startswith("__version__"):
                delim = '"' if '"' in line else "'"
                return line.split(delim)[1]
    raise RuntimeError(f"Unable to find version string in {filepath}.")


__version__ = get_version(Path(__file__).parent / "finalyzer/__init__.py")
install_reqs = parse_requirements(Path(__file__).parent / "requirements.txt")
dev_reqs = parse_requirements(Path(__file__).parent / "requirements_dev.txt")

if __name__ == "__main__":

    setup(
        name="finalyzer",
        version=__version__,
        license="proprietary",
        long_description=open("README.md").read(),
        long_description_content_type="text/markdown",
        packages=find_packages(),
        install_requires=install_reqs,
        extras_require={"dev": dev_reqs},
        python_requires=">=3.9",
        entry_points={"console_scripts": ["finalyze = finalyzer.cli:cli"]},
    )
